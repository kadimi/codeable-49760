<?php

/**
 * Add questions categories.
 */
add_action( 'init', function() {

	$labels = array(
		'name'              => _x( 'Categories', 'taxonomy general name' ),
		'singular_name'     => _x( 'Category', 'taxonomy singular name' ),
		'search_items'      => __( 'Search Categories' ),
		'all_items'         => __( 'All Categories' ),
		'parent_item'       => __( 'Parent Category' ),
		'parent_item_colon' => __( 'Parent Category:' ),
		'edit_item'         => __( 'Edit Category' ),
		'update_item'       => __( 'Update Category' ),
		'add_new_item'      => __( 'Add New Category' ),
		'new_item_name'     => __( 'New Category Name' ),
		'menu_name'         => __( 'Category' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'question_category' ),
	);

	register_taxonomy( 'lp_question_category', 'lp_question', $args );

	add_action( 'learn_press_menu_items', function( $menu_items ) {
		return array_merge( $menu_items, [
			'question_categories' => array(
				'learn_press',
				'',
				__( 'Questions', 'learnpress' ) . ' ❭ ' . __( 'Categories' ),
				'manage_options',
				'redirect-to-questions-categories',
				'codeable_49760_redirect_to_questions_categories'
			)
		] );
	} );

	if ( strstr( $_SERVER['REQUEST_URI'], 'redirect-to-questions-categories' ) ) {
		codeable_49760_redirect_to_questions_categories();
	}
} );

function codeable_49760_redirect_to_questions_categories() {
	wp_redirect( admin_url( 'edit-tags.php?taxonomy=lp_question_category&post_type=lp_course' ) );
}