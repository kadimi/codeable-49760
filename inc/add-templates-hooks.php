<?php

add_action( 'learn_press_before_template_part', function( $template ) {
	do_action( $template . '::start', $template );
} );

add_action( 'learn_press_after_template_part', function( $template ) {
	do_action( $template . '::end' );
} );

add_action( 'x.all', function() {

	$f = current_filter();

	/**
	 * Only on learnpress template hooks.
	 */
	if ( preg_match( '/\.php::(start|end)$/', $f ) ) {
		if ( preg_match( '/question/', $f ) ) {
			var_dump( current_filter() );
		}
	}
} );
