<?php

add_action( 'init', function() {
	add_action( 'content-quiz/question-content.php::start', function() {
		ob_start();
	} );
	add_action( 'content-quiz/question-content.php::end', function() {

		$content = ob_get_clean();

		$user   = learn_press_get_current_user();
		$course = LP()->global['course'];
		$quiz   = isset( $item ) ? $item : LP()->global['course-item'];
		if ( $quiz ) {
			$question_id = $user->get_current_quiz_question( $quiz->id, $course->id );
			if ( $question_id ) {
				$question_categories = wp_get_post_terms( $question_id, 'lp_question_category', [ 'fields' => 'names' ] );
				$question_categories_str = implode( ' | ', $question_categories );
				$content = str_replace(
					'<h4 class="quiz-question-title">',
					'<h4 class="quiz-question-title">' . (
						$question_categories_str
							? sprintf( '<small>%s</small> ❭ ', $question_categories_str )
							: ''
					),
					$content
				);
			}
		}
		echo $content;
	} );
} );
