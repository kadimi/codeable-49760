<?php

add_action( 'init', function() {


	add_action( 'content-quiz/result.php::start', function() {
		ob_start();
	} );
	add_action( 'content-quiz/result.php::end', function() {

		$content = ob_get_clean();

		$user   = learn_press_get_current_user();
		$course = LP()->global['course'];
		$quiz   = LP()->global['course-item'];
		if ( !$user->has( 'completed-quiz', $quiz->id ) ) {
			echo $content;
			return;
		}

		/**
		 * Exit function if quiz is not set to have categorized result.
		 */
		$show_result_categorized = ( Boolean ) get_post_meta( $quiz->id, 'codeable_49760_group_results_by_categories', true );
		if ( ! $show_result_categorized ) {
			echo $content;
			return;
		}

		/**
		 * Categorize result.
		 */
		$history = $user->get_quiz_results( $quiz->id );
		$result_categorized = [];
		foreach ( $history->answer_results as $question_id => $question_result ) {
			$question_categories = wp_get_post_terms( $question_id, 'lp_question_category',  [ 'fields' => 'names' ] );
			foreach ( $question_categories as $question_category ) {
				if ( ! array_key_exists( $question_category, $result_categorized ) ) {
					$result_categorized[ $question_category ] = [
						'correct' => 0,
						'total' => 0,
					];
				}
				$result_categorized[ $question_category ]['total']++;
				if ( ! empty ( $question_result['correct'] ) ) {
					$result_categorized[ $question_category ]['correct']++;
				}
			}
		}

		/**
		 * Exit function if ther is no data to categorize.
		 */
		if ( ! $result_categorized ) {
			echo $content;
			return;
		}

		/**
		 * A helper array for categirized result.
		 *
		 * - Category names as keys
		 * - Percent total as values
		 *
		 * @var Array
		 */
		$chart_data = array_map( function( $el ) { return round( 100 * $el['correct']/$el['total'] ); }, $result_categorized );

		/**
		 * Build HTML
		 */
		if ( $result_categorized ) {
			foreach ( $result_categorized as $c => $t) {
				$s = sprintf( '%s %d/%d (%d%%)', $c, $t['correct'], $t['total'], 100 * $t['correct'] / $t['total'] );
				$result_categorized_output .= sprintf(
					'
						<div class="quiz-result-field empty" data-value="%1$d">
							%2$s <span data-text="%2$s"></span>
						</div>
					'
					, 100
					, $s
				);
			}
		}
		$result_categorized_output = sprintf( '<h5>%s</h5><div class="quiz-result-summary">%s</div>'
			, __( 'Categories', starter()->plugin_slug )
			, $result_categorized_output
		);

		/**
		 * Calculate ideal font size
		 *
		 * The standard is:
		 * - 15px for 10 letters
		 * - 12px as minumum for 30 letters and beyond
		 */
		$longest_label = max( array_map( 'strlen', array_keys( $chart_data ) ) );
		$font_size = 15;
		if ( $longest_label > 10 ) {
			$font_size *= 15 / $longest_label;
		}
		if ( $font_size < 12 ) {
			$font_size = 12;
		}

		/**
		 * The chart.
		 */
		ob_start();
		?>
			<canvas id="quiz-result-categorized-chart"></canvas>
			<script>
			jQuery( document ).ready( function( $ ) {
				new Chart( document.getElementById("quiz-result-categorized-chart"), {
					type: "radar",
					data: {
						labels: ["<?php echo implode('","',  array_keys( $chart_data ) ) ?>"],
						datasets: [
							{
								label: '',
								data: [<?php echo implode(',',  array_values( $chart_data ) ) ?>]
							},
							{
								label: '',
								data: [<?php echo preg_replace( '/\d+/', '0', implode(',',  array_values( $chart_data ) ) ); ?>]
							}
						]
					},
					options: {
						scale: {
							ticks: {
								min: 0,
								max: 100,
								max: 100,
								stepSize: 20,
								beginAtZero: true
							},
							pointLabels: {
								fontSize: <?php echo $font_size ?>
							}
						},
						legend: {
							display: false
						}
					}
				});
			} );
			</script>
		<?php
		$result_categorized_output .= ob_get_clean();

		/**
		 * Location before which I will try to inject the new HTML. 
		 * @var Array.
		 */
		$locations = [
			'<p class="quiz-result-time">',
			'<div class="quiz-grade">',
			'<div class="clearfix">',
		];

		/**
		 * Try to inject our html, exiting the loop at the first successful replacement.
		 */
		reset( $locations );
		do {
			$location = current( $locations );
			$tmp = str_replace_first( $location, $result_categorized_output . $location, $content );
			if ( $tmp !== $content ) {
				$content = $tmp;
				wp_enqueue_script( 'Chart.js', 'https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js', [ 'jquery' ], null, true );
				break;
			}
		}
		while ( next( $locations ) );

		/**
		 * Output.
		 */
		echo $content;
	} );
} );

function str_replace_first( $from, $to, $subject ) {
	$from = '/' . preg_quote( $from, '/' ). '/';
	return preg_replace( $from, $to, $subject, 1 );
}
