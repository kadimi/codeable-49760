<?php

add_action( 'init', function() {
	register_field_group( [
		'id'         => 'acf_questions-categories',
		'title'      => __( 'Questions', 'learnpress' ) . ' › ' . __( 'Categories' ),
		'fields'     => [
			[
				'key'     => 'codeable_49760_group_results_by_categories',
				'label'   => __( 'Group results by categories?', starter()->plugin_slug ),
				'name'    => 'codeable_49760_group_results_by_categories',
				'type'    => 'true_false',
				'message' => __( 'Yes' ),
			],
		],
		'location'   => [
			[
				[
					'param'    => 'post_type',
					'operator' => '==',
					'value'    => 'lp_quiz',
				],
			],
		],
		'menu_order' => 0,
		'options'    => [
			'hide_on_screen' => [],
			'layout'         => 'default',
			'position'       => 'side',
		],
	] );
} );
